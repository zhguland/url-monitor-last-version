package com.bahmet.urlmonitor.service;

import com.bahmet.urlmonitor.dto.MonitoredEndpoint.*;
import com.bahmet.urlmonitor.repository.model.MonitoredEndpoint;

import java.time.LocalDateTime;
import java.util.List;

public interface MonitoredEndpointServiceInterface {

    List<MonitoredEndpointResponseDTO> getAllMonitoredEndpoints(String userId);

    MonitoredEndpointWithResultsResponseDTO getMonitoredEndpointById(Long id, String userId);

    MonitoredEndpointResponseDTO createMonitoredEndpoint(MonitoredEndpointRequestDTO monitoredEndpointRequestDTO, String userId);

    void createMonitoredEndpointCollection(MonitoredEndpointCollectionRequestDTO monitoredEndpointCollectionRequestDTO, String userId);

    MonitoredEndpointResponseDTO updateMonitoredEndpoint(Long id, MonitoredEndpointRequestDTO monitoredEndpointRequestDTO, String userId);

    MonitoredEndpointResponseDTO updateMonitoredEndpointPartially(Long id, MonitoredEndpointPartiallyUpdateRequestDTO monitoredEndpointPartiallyUpdateRequestDTO, String userId);

    void deleteMonitoredEndpoint(Long id, String userId);

    List<MonitoredEndpoint> getEndpointsToCheck(LocalDateTime checkDateTime);
}
