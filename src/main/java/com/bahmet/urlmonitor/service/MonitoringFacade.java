package com.bahmet.urlmonitor.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MonitoringFacade {

    private final MonitoringSchedulerService monitoringSchedulerService;

    public void performMonitoring() {
        monitoringSchedulerService.checkMonitoredEndpoints();
    }
}