package com.bahmet.urlmonitor.service;

import com.bahmet.urlmonitor.dto.MonitoredEndpoint.*;
import com.bahmet.urlmonitor.repository.model.MonitoredEndpoint;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class MonitoredEndpointServiceProxy implements MonitoredEndpointServiceInterface {

    private final MonitoredEndpointService monitoredEndpointService;

    @Override
    public List<MonitoredEndpointResponseDTO> getAllMonitoredEndpoints(String userId) {
        log.info("Fetching all endpoints for user {}", userId);
        List<MonitoredEndpointResponseDTO> result = monitoredEndpointService.getAllMonitoredEndpoints(userId);
        log.info("Fetched {} endpoints for user {}", result.size(), userId);
        return result;
    }

    @Override
    public MonitoredEndpointWithResultsResponseDTO getMonitoredEndpointById(Long id, String userId) {
        log.info("Fetching endpoint with id {} for user {}", id, userId);
        MonitoredEndpointWithResultsResponseDTO result = monitoredEndpointService.getMonitoredEndpointById(id, userId);
        log.info("Fetched endpoint with id {} for user {}", id, userId);
        return result;
    }

    @Override
    public MonitoredEndpointResponseDTO createMonitoredEndpoint(MonitoredEndpointRequestDTO monitoredEndpointRequestDTO, String userId) {
        log.info("Creating endpoint for user {}", userId);
        MonitoredEndpointResponseDTO result = monitoredEndpointService.createMonitoredEndpoint(monitoredEndpointRequestDTO, userId);
        log.info("Created endpoint with id {} for user {}", result.getId(), userId);
        return result;
    }

    @Override
    public void createMonitoredEndpointCollection(MonitoredEndpointCollectionRequestDTO monitoredEndpointCollectionRequestDTO, String userId) {
        log.info("Creating endpoint collection for user {}", userId);
        monitoredEndpointService.createMonitoredEndpointCollection(monitoredEndpointCollectionRequestDTO, userId);
        log.info("Created endpoint collection for user {}", userId);
    }

    @Override
    public MonitoredEndpointResponseDTO updateMonitoredEndpoint(Long id, MonitoredEndpointRequestDTO monitoredEndpointRequestDTO, String userId) {
        log.info("Updating endpoint with id {} for user {}", id, userId);
        MonitoredEndpointResponseDTO result = monitoredEndpointService.updateMonitoredEndpoint(id, monitoredEndpointRequestDTO, userId);
        log.info("Updated endpoint with id {} for user {}", id, userId);
        return result;
    }

    @Override
    public MonitoredEndpointResponseDTO updateMonitoredEndpointPartially(Long id, MonitoredEndpointPartiallyUpdateRequestDTO monitoredEndpointPartiallyUpdateRequestDTO, String userId) {
        log.info("Partially updating endpoint with id {} for user {}", id, userId);
        MonitoredEndpointResponseDTO result = monitoredEndpointService.updateMonitoredEndpointPartially(id, monitoredEndpointPartiallyUpdateRequestDTO, userId);
        log.info("Partially updated endpoint with id {} for user {}", id, userId);
        return result;
    }

    @Override
    public void deleteMonitoredEndpoint(Long id, String userId) {
        log.info("Deleting endpoint with id {} for user {}", id, userId);
        monitoredEndpointService.deleteMonitoredEndpoint(id, userId);
        log.info("Endpoint with id {} deleted successfully", id);
    }

    @Override
    public List<MonitoredEndpoint> getEndpointsToCheck(LocalDateTime checkDateTime) {
        log.info("Getting endpoints to check");
        List<MonitoredEndpoint> result = monitoredEndpointService.getEndpointsToCheck(checkDateTime);
        log.info("Found {} endpoints to check", result.size());
        return result;
    }
}