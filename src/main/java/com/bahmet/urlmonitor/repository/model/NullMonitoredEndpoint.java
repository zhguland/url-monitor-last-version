package com.bahmet.urlmonitor.repository.model;

import lombok.Getter;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Getter
public class NullMonitoredEndpoint extends MonitoredEndpoint {

    public NullMonitoredEndpoint() {
        this.setId(-1L);
        this.setName("N/A");
        this.setUrl("N/A");
        this.setDateOfCreation(LocalDateTime.MIN);
        this.setDateOfLastCheck(LocalDateTime.MIN);
        this.setMonitoredInterval(0L);
        this.setUserId("N/A");
        this.setMonitoringResults(Collections.emptyList());
    }

    @Override
    public List<MonitoringResult> getMonitoringResults() {
        return Collections.emptyList();
    }
}