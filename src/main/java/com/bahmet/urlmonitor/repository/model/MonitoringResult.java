package com.bahmet.urlmonitor.repository.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDateTime;

@Entity
@Table(name = "monitoring_results")
@Document(indexName = "monitoring_results")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_of_check")
    @Field(type = FieldType.Date)
    private LocalDateTime dateOfCheck;

    @Column(name = "http_status_code")
    @Field(type = FieldType.Integer)
    private Integer httpStatusCode;

    @Column(name = "payload", columnDefinition = "MEDIUMTEXT")
    @Field(type = FieldType.Text)
    private String payload;

    @ManyToOne
    @JoinColumn(name = "monitored_endpoint_id")
    @Transient
    private MonitoredEndpoint monitoredEndpoint;
}
