import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import EndpointList from './components/EndpointList';
import EndpointDetails from './components/EndpointDetails';
import AddEditEndpoint from './components/AddEditEndpoint';
import { Alert } from '@mui/material';
import ErrorBoundary from './components/ErrorBoundary';


function App() {
    const [globalError, setGlobalError] = useState(null);

    // Функция для обработки ошибок на уровне приложения
    const handleGlobalError = (error) => {
        console.error('Глобальная ошибка:', error);
        setGlobalError('Произошла критическая ошибка. Попробуйте перезагрузить страницу.');
    };

    return (
        <ErrorBoundary>
            <Router>
                {globalError && (
                    <Alert severity="error" onClose={() => setGlobalError(null)}>
                        {globalError}
                    </Alert>
                )}
                <Routes>
                    <Route
                        path="/"
                        element={<EndpointList onError={handleGlobalError} />}
                    />
                    <Route
                        path="/endpoints/:id"
                        element={<EndpointDetails onError={handleGlobalError} />}
                    />
                    <Route
                        path="/add"
                        element={<AddEditEndpoint onError={handleGlobalError} />}
                    />
                    <Route
                        path="/edit/:id"
                        element={<AddEditEndpoint onError={handleGlobalError} />}
                    />
                </Routes>
            </Router>
        </ErrorBoundary>
    );
}

export default App;