import axios from 'axios';

const API_BASE_URL = '/api/monitored-endpoints';

const getAllEndpoints = async () => {
    try {
        const response = await axios.get(API_BASE_URL);
        return response.data;
    } catch (error) {
        console.error('Ошибка при получении списка URL:', error);
        throw error; // Проброс ошибки для обработки на уровне компонента
    }
};

const getEndpoint = async (id) => {
    try {
        const response = await axios.get(`${API_BASE_URL}/${id}`);
        return response.data;
    } catch (error) {
        console.error('Ошибка при получении URL:', error);
        throw error;
    }
};

const createEndpoint = async (endpointData) => {
    try {
        const response = await axios.post(API_BASE_URL, endpointData);
        return response.data;
    } catch (error) {
        console.error('Ошибка при создании URL:', error);
        throw error;
    }
};

const updateEndpoint = async (id, endpointData) => {
    try {
        const response = await axios.put(`${API_BASE_URL}/${id}`, endpointData);
        return response.data;
    } catch (error) {
        console.error('Ошибка при обновлении URL:', error);
        throw error;
    }
};

const deleteEndpoint = async (id) => {
    try {
        await axios.delete(`${API_BASE_URL}/${id}`);
    } catch (error) {
        console.error('Ошибка при удалении URL:', error);
        throw error;
    }
};

const endpointService = {
    getAllEndpoints,
    getEndpoint,
    createEndpoint,
    updateEndpoint,
    deleteEndpoint
};

export default endpointService;