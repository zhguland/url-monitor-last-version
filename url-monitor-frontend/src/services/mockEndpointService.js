import endpoints from '../mocks/endpoints';

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const getAllEndpoints = async () => {
    await sleep(500); // Имитация задержки сервера
    return endpoints;
};

const getEndpoint = async (id) => {
    await sleep(500);
    const endpoint = endpoints.find((e) => e.id === parseInt(id));
    if (!endpoint) {
        throw new Error('Endpoint not found');
    }
    return endpoint;
};

const createEndpoint = async (endpointData) => {
    await sleep(500);
    const newEndpoint = { id: Date.now(), ...endpointData };
    endpoints.push(newEndpoint);
    return newEndpoint;
};

const updateEndpoint = async (id, endpointData) => {
    await sleep(500);
    const index = endpoints.findIndex((e) => e.id === parseInt(id));
    if (index === -1) {
        throw new Error('Endpoint not found');
    }
    endpoints[index] = { ...endpoints[index], ...endpointData };
    return endpoints[index];
};

const deleteEndpoint = async (id) => {
    await sleep(500);
    const index = endpoints.findIndex((e) => e.id === parseInt(id));
    if (index === -1) {
        throw new Error('Endpoint not found');
    }
    endpoints.splice(index, 1);
};

const mockEndpointService = {
    getAllEndpoints,
    getEndpoint,
    createEndpoint,
    updateEndpoint,
    deleteEndpoint
};

export default mockEndpointService;