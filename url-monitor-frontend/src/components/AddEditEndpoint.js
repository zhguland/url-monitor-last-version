import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { TextField, Button, Grid, Typography, Alert } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import endpointService from '../services/endpointService';
import mockEndpointService from "../services/mockEndpointService";

const AddEditEndpoint = () => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    const navigate = useNavigate();
    const { id } = useParams(); // Получаем ID из URL, если он есть (режим редактирования)
    const [submitError, setSubmitError] = useState(null);

    useEffect(() => {
        // Если это режим редактирования, загружаем данные URL
        if (id) {
            const fetchEndpoint = async () => {
                try {
                    const endpoint = await endpointService.getEndpoint(id);
                    reset(endpoint); // Заполняем форму данными URL
                } catch (error) {
                    console.error('Ошибка при загрузке URL:', error);
                    setSubmitError('Ошибка при загрузке URL. Попробуйте позже.');
                }
            };
            fetchEndpoint();
        }
    }, [id, reset]);

    const onSubmit = async (data) => {
        setSubmitError(null); // Сбрасываем ошибку при новой отправке

        try {
            if (id) {
                await mockEndpointService.updateEndpoint(id, data); // Мок-сервис для обновления
            } else {
                await mockEndpointService.createEndpoint(data); // Мок-сервис для создания
            }
            navigate('/');
        } catch (error) {
            console.error('Ошибка при отправке формы:', error);
            setSubmitError(error.message || 'Произошла ошибка. Попробуйте позже.');
        }
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography variant="h6">
                        {id ? 'Редактировать URL' : 'Добавить новый URL'}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        {...register('name', { required: 'Название обязательно' })}
                        label="Название"
                        fullWidth
                        error={Boolean(errors.name)}
                        helperText={errors.name?.message}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        {...register('url', { required: 'URL обязателен' })}
                        label="URL"
                        fullWidth
                        error={Boolean(errors.url)}
                        helperText={errors.url?.message}
                    />
                </Grid>
                {/* Добавьте другие поля формы по мере необходимости */}

                {submitError && (
                    <Grid item xs={12}>
                        <Alert severity="error" onClose={() => setSubmitError(null)}>
                            {submitError}
                        </Alert>
                    </Grid>
                )}

                <Grid item xs={12}>
                    <Button type="submit" variant="contained">
                        {id ? 'Сохранить' : 'Добавить'}
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default AddEditEndpoint;