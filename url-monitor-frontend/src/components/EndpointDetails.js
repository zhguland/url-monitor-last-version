import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import endpointService from '../services/endpointService';
import { Typography } from '@mui/material';
import { useLocation } from 'react-router-dom';

const EndpointDetails = () => {
    const location = useLocation();
    const endpoint = location.state?.endpoint;
    const { id } = useParams();
    const [setEndpoint] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchEndpoint = async () => {
            try {
                const data = await endpointService.getEndpoint(id);
                setEndpoint(data);
            } catch (err) {
                setError(err.message);
            } finally {
                setLoading(false);
            }
        };

        fetchEndpoint();
    }, [id]);

    if (loading) {
        return <Typography>Загрузка...</Typography>;
    }

    if (error) {
        return <Typography color="error">Ошибка: {error}</Typography>;
    }

    if (!endpoint) {
        return <Typography>URL не найден</Typography>;
    }

    return (
        <div>
            <h2>Детали URL</h2>
            <Typography><strong>ID:</strong> {endpoint.id}</Typography>
            <Typography><strong>Название:</strong> {endpoint.name}</Typography>
            <Typography><strong>URL:</strong> {endpoint.url}</Typography>
            <h3>Результаты мониторинга:</h3>
            {endpoint.monitoringResults.map((result) => (
                <div key={result.id}>
                    <p>Дата проверки: {result.dateOfCheck}</p>
                    <p>HTTP статус: {result.httpStatusCode}</p>
                    <p>Ответ сервера: {result.payload}</p>
                </div>
            ))}
        </div>
    );
};

export default EndpointDetails;