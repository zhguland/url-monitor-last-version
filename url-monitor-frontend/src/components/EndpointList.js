import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import endpointService from '../services/endpointService';
import mockEndpointService from '../services/mockEndpointService';
import { useNavigate } from 'react-router-dom';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Button,
    Typography
} from '@mui/material';


const EndpointList = () => {
    const [endpoints, setEndpoints] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const navigate = useNavigate();


    useEffect(() => {
        const fetchEndpoints = async () => {
            try {
                const data = await mockEndpointService.getAllEndpoints();
                setEndpoints(data);
            } catch (err) {
                setError(err.message);
            } finally {
                setLoading(false);
            }
        };

        fetchEndpoints();
    }, []);

    const handleDelete = async (id) => {
        try {
            await mockEndpointService.deleteEndpoint(id); // Вызываем мок-сервис
            setEndpoints(endpoints.filter(endpoint => endpoint.id !== id));
        } catch (error) {
            console.error('Ошибка при удалении URL:', error);
        }
    };

    if (loading) {
        return <Typography>Загрузка...</Typography>;
    }

    if (error) {
        return <Typography color="error">Ошибка: {error}</Typography>;
    }

    return (
        <div>
            <h2>Отслеживаемые URL</h2>
            <Link to="/add">
                <Button variant="contained">Добавить URL</Button>
            </Link>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Название</TableCell>
                            <TableCell>URL</TableCell>
                            <TableCell>Действия</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {endpoints.map((endpoint) => (
                            <TableRow key={endpoint.id}>
                                <TableCell>{endpoint.id}</TableCell>
                                <TableCell>{endpoint.name}</TableCell>
                                <TableCell>{endpoint.url}</TableCell>
                                <TableCell>
                                    <Link to={`/endpoints/${endpoint.id}`}>
                                        <button
                                            onClick={() => navigate(`/endpoints/${endpoint.id}`, {state: {endpoint}})}>
                                            Детали
                                        </button>
                                    </Link>
                                    <Link to={`/edit/${endpoint.id}`}>
                                        <Button variant="outlined" size="small">
                                            Редактировать
                                        </Button>
                                    </Link>
                                    <Button
                                        variant="outlined"
                                        size="small"
                                        color="error"
                                        onClick={() => handleDelete(endpoint.id)}
                                    >
                                        Удалить
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default EndpointList;