import React, {useState} from 'react';
import { useForm } from 'react-hook-form';
import { TextField, Button, Grid, Typography, Alert } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import endpointService from '../services/endpointService';

const EndpointForm = () => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    const navigate = useNavigate();
    const { id } = useParams();
    const [submitError, setSubmitError] = useState(null);

    const onSubmit = async (data) => {
        setSubmitError(null); // Сбрасываем ошибку при новой отправке
        try {
            if (id) {
                await endpointService.updateEndpoint(id, data);
            } else {
                await endpointService.createEndpoint(data);
            }
            navigate('/');
        } catch (error) {
            console.error('Ошибка при отправке формы:', error);
            setSubmitError(error.message || 'Произошла ошибка. Попробуйте позже.');
        }
    };

    // Fetch endpoint details for editing (if id exists)
    React.useEffect(() => {
        const fetchEndpoint = async () => {
            try {
                const endpoint = await endpointService.getEndpoint(id);
                reset(endpoint); // Populate form fields
            } catch (error) {
                console.error('Error fetching endpoint:', error);
            }
        };

        if (id) {
            fetchEndpoint();
        }
    }, [id, reset]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography variant="h6">
                        {id ? 'Edit Endpoint' : 'Add New Endpoint'}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        {...register('name', { required: 'Name is required' })}
                        label="Name"
                        fullWidth
                        error={Boolean(errors.name)}
                        helperText={errors.name?.message}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        {...register('url', { required: 'URL is required' })}
                        label="URL"
                        fullWidth
                        error={Boolean(errors.url)}
                        helperText={errors.url?.message}
                    />
                </Grid>

                {submitError && (
                    <Alert severity="error" onClose={() => setSubmitError(null)}>
                        {submitError}
                    </Alert>
                )}

                <Grid item xs={12}>
                    <Button type="submit" variant="contained">
                        {id ? 'Update' : 'Create'}
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default EndpointForm;