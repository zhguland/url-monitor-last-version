const endpoints = [
    {
        id: 1,
        name: 'Example Endpoint 1',
        url: 'https://example.com',
        dateOfCreation: '2023-12-18T10:00:00.000Z',
        dateOfLastCheck: '2023-12-18T10:10:00.000Z',
        monitoredInterval: 60,
        monitoringResults: [
            {
                id: 1,
                dateOfCheck: '2023-12-18T10:10:00.000Z',
                httpStatusCode: 200,
                payload: 'OK'
            }
        ]
    },
];

export default endpoints;