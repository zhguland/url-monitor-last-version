const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(
        '/api', // Префикс для проксирования запросов на бэкенд
        createProxyMiddleware({
            target: 'http://localhost:8080', // Адрес твоего бэкенда
            changeOrigin: true,
        })
    );
};