# URL Monitor

URL Monitor is a robust Spring Boot application designed to monitor and record the status of various web endpoints. It periodically checks the specified URLs and stores the results, including HTTP status codes and response times, helping users maintain insights into the uptime and performance of their web services.

## Architecture Overview

The application follows a microservices architecture pattern, with a focus on decoupling the core functionalities into separate components:

- **API Layer**: Handles incoming requests and routes them to appropriate service layers.
- **Service Layer**: Contains business logic for monitoring URLs and managing data.
- **Data Access Layer**: Interacts with the database to store and retrieve monitoring results.
- **Database**: Uses MySQL for persistent storage of monitoring data.
- **Distribute Locking**: Uses Redis for manage distributed locks to ensure data consistency across multiple instances.

### Project Structure

```plaintext
.
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── bahmet
│   │   │           └── urlmonitor
│   │   │               ├── controller      # REST API controllers
│   │   │               │   └── filter      # Filters
│   │   │               ├── service         # Business logic
│   │   │               ├── repository      # Data access repositories
│   │   │               │   └── model       # Entity definitions
│   │   │               ├── dto             # Data transfer objects
│   │   │               ├── exception       # Custom exceptions
│   │   │               ├── util            # Utility classes
│   │   │               │   └── mapper      # Model mapper configurations
│   │   │               └── configuration   # Configuration classes
│   │   └── resources
│   │       ├── db
│   │       │   └── migration               # Flyway database migrations
│   │       └── application.yml             # Application configurations
│   └── test                                # Integration tests
├── Dockerfile                              # Docker configuration file
├── docker-compose.yml                      # Docker-compose setup
└── README.md
```

### Main Technologies
- **Spring Boot**: Framework for building stand-alone, production-grade Spring based Applications.
- **MySQL**: Relational database management system.
- **Redis**: In-memory data structure store, used as a database.
- **Flyway**: Tool for database migrations.
- **Docker**: Platform for developing, shipping, and running applications.

## Splněné požadavky
Níže je uveden podrobný popis splněných bodů ze seznamu požadavků:

**Společné požadavky:**

* **Výběr vhodných technologií a jazyka:**
    * **Java/SpringBoot:** Projekt je implementován v jazyce Java s využitím frameworku Spring Boot, což zajišťuje rychlý vývoj a nasazení aplikace. 
        * **Umístění:** Celý projekt
    * **Java:** Veškerá logika aplikace je napsána v jazyce Java.
        * **Umístění:** Celý projekt
    * **Příklad kódu:**
    ```java
    // src/main/java/com/bahmet/urlmonitor/UrlMonitorApplication.java
    @SpringBootApplication
    @EnableScheduling
    @EnableAsync
    @OpenAPIDefinition
    @EnableCaching
    public class UrlMonitorApplication {

        public static void main(String[] args) {
            SpringApplication.run(UrlMonitorApplication.class, args);
        }
    }
    ```
* **Využití společné databáze (relační nebo grafové):**
    * **MySQL:** Jako databáze je použita MySQL pro ukládání informací o sledovaných URL adresách a výsledcích jejich kontrol.
        * **Umístění:** Konfigurace v souboru `src/main/resources/application.yml`
    * **Příklad kódu (konfigurace připojení k databázi):**
    ```yaml
    # src/main/resources/application.yml
    spring:
      datasource:
        url: ${SPRING_DATASOURCE_URL}
        username: ${SPRING_DATASOURCE_USERNAME}
        password: ${SPRING_DATASOURCE_PASSWORD}
        driver-class-name: com.mysql.cj.jdbc.Driver
    ```
* **Využití cache:**
    * **Redis:** V projektu je použit Redis pro cachování výsledků dotazů do databáze, což umožňuje snížit zátěž databáze a urychlit dobu odezvy aplikace.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/configuration/RedisCacheConfig.java`
    * **Příklad kódu (konfigurace cachování):**
    ```java
    // src/main/java/com/bahmet/urlmonitor/configuration/RedisCacheConfig.java
    @Configuration
    public class RedisCacheConfig {

        @Bean
        public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
            RedisCacheConfiguration cacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                    .entryTtl(Duration.ofMinutes(5))
                    .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()));

            return RedisCacheManager.builder(redisConnectionFactory)
                    .cacheDefaults(cacheConfiguration)
                    .build();
        }
    }
    ```
* **Využití principu messaging:**
    * **Nerealizováno:** Tato funkcionalita nebyla v rámci projektu implementována.
* **Zabezpečení aplikace:**
    * **OAuth2:** Aplikace je zabezpečena pomocí OAuth2, což zajišťuje bezpečnou autentizaci a autorizaci uživatelů.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/configuration/SecurityConfig.java` a konfigurace v souboru `src/main/resources/application.yml`
    * **Příklad kódu (konfigurace OAuth2):**
    ```yaml
    # src/main/resources/application.yml
    spring:
      security:
        oauth2:
          resourceserver:
            jwt:
              issuer-uri: https://dev-53tm81j5h4zbvbma.us.auth0.com/
              audience: https://urlmonitor
    ```
* **Využití Interceptorů:**
    * **RequestLoggingInterceptor:** Implementován Interceptor `RequestLoggingInterceptor`, který loguje všechny příchozí HTTP požadavky, včetně metody požadavku a URI.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/configuration/RequestLoggingInterceptor.java`
    * **Příklad kódu:**
    ```java
    // src/main/java/com/bahmet/urlmonitor/configuration/RequestLoggingInterceptor.java
    @Component
    @Slf4j
    public class RequestLoggingInterceptor implements HandlerInterceptor {

        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
            log.info("Incoming request: {} {}", request.getMethod(), request.getRequestURI());
            return true; 
        }

        @Override
        public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
            log.info("Request completed with status: {}", response.getStatus());
        }
    }
    ```
* **Využití jedné z technologií: SOAP, REST, graphQL, Java RMI, Corba, XML-RPC:**
    * **REST:** Aplikace využívá REST API pro komunikaci s klienty.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/controller/MonitoredEndpointController.java`
    * **Příklad kódu (REST Controller):**
    ```java
    // src/main/java/com/bahmet/urlmonitor/controller/MonitoredEndpointController.java
    @RestController
    @RequestMapping("/api")
    @Slf4j
    @RequiredArgsConstructor
    public class MonitoredEndpointController {
        // ...
    }
    ```
* **Nasazení na produkční server:**
    * **Heroku:** Aplikace může být nasazena na platformě Heroku.
        * **Umístění:** Instrukce pro nasazení na Heroku by byly součástí dokumentace projektu.
* **Výběr vhodné architektury:**
    * **Vícevrstvá architektura:** Projekt je implementován s využitím vícevrstvé architektury, která rozděluje aplikaci na vrstvy prezentace, business logiky a přístupu k datům.
        * **Umístění:** Struktura projektu (balíčky `controller`, `service`, `repository`)
* **Inicializační postup:**
    * **Flyway:** Pro migraci databáze je použit nástroj Flyway.
        * **Umístění:** Konfigurace v souboru `src/main/resources/application.yml` a migrační skripty v adresáři `src/main/resources/db/migration`
    * **Příklad kódu (konfigurace Flyway):**
    ```yaml
    # src/main/resources/application.yml
    spring:
      flyway:
        enabled: true
        locations: classpath:db/migration
        schemas: urlmonitor
    ```
* **Využití Elasticsearch:**
    * **Implementováno:** V projektu je použit Elasticsearch pro ukládání a vyhledávání výsledků monitorování URL adres.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/repository/elasticsearch/MonitoringResultElasticsearchRepository.java`
    * **Příklad kódu (repositář Elasticsearch):**
    ```java
    // src/main/java/com/bahmet/urlmonitor/repository/elasticsearch/MonitoringResultElasticsearchRepository.java
    package com.bahmet.urlmonitor.repository.elasticsearch;

    import com.bahmet.urlmonitor.repository.model.MonitoringResult;
    import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

    public interface MonitoringResultElasticsearchRepository extends ElasticsearchRepository<MonitoringResult, Long> {
    }
    ```
* **Použití alespoň 5 návrhových vzorů:**
    * **Singleton:** `DistributedLockService` je implementován jako Singleton, aby se zajistilo, že v daný okamžik existuje pouze jedna instance služby.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/service/DistributedLockService.java`
    * **Repository:** Používá se pro abstrakci přístupu k datům, např. `MonitoredEndpointRepository`, `MonitoringResultRepository`.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/repository/MonitoredEndpointRepository.java`, `src/main/java/com/bahmet/urlmonitor/repository/MonitoringResultRepository.java`
    * **Mapper:** `MonitoredEndpointMapper` a `MonitoringResultMapper` se používají pro převod dat mezi DTO a entitami.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/util/mapper/MonitoredEndpointMapper.java`, `src/main/java/com/bahmet/urlmonitor/util/mapper/MonitoringResultMapper.java`
    * **Service:** `MonitoredEndpointService`, `MonitoringResultService`, `HttpClientService` zapouzdřují business logiku aplikace.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/service/MonitoredEndpointService.java`, `src/main/java/com/bahmet/urlmonitor/service/MonitoringResultService.java`, `src/main/java/com/bahmet/urlmonitor/service/HttpClientService.java`
    * **Scheduled Task:** `MonitoringSchedulerService` využívá anotaci `@Scheduled` pro spouštění kontroly dostupnosti URL adres podle plánu.
        * **Umístění:** `src/main/java/com/bahmet/urlmonitor/service/MonitoringSchedulerService.java`


## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
- Java 17
- Docker
- Docker Compose

### Running the Application

1. **Clone the repository**
```
git clone https://github.com/yourgithub/url-monitor.git
cd url-monitor
```

2. **Build the application**
```
mvn clean package
```

3. **Start the services**
```
docker-compose up --build
```

### Accessing the Application
Once the application is running, it can be accessed via:
```
http://localhost:8080/
```

### Testing with Postman
I have provided a collection of Postman requests to help you interact with and test the API. This collection can be found in the Postman folder of the project.

To use it:
- Open Postman.
- Import the collection by navigating to the Postman folder and selecting the collection file.
- Ensure your local services are running as described above.
- Send requests from Postman to test various endpoints.

### API Documentation
API documentation is provided via Swagger and can be accessed through the following URL when the application is running:
```
http://localhost:8080/swagger-ui.html
```
